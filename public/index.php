<?php

require_once('../vendor/autoload.php');

// use \OneFrameLink\App;
// dump(get_declared_classes());
date_default_timezone_set("America/New_York");

// App($production = true, $queryLogging = false)
$app = new \OneFrameLink\App(false, false);

$app->dispatch();
