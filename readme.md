# OneFrameLink
***

OneFrameLink is a tiny MVC framework for simple PHP applications.  It follows the [PSR-4](http://www.php-fig.org/psr/psr-4/) and [PSR-7](http://www.php-fig.org/psr/psr-7/) PHP-FIG specifications.

## Getting started
***

OFL is not currently on Packagist, so it needs to be included in your composer.json file manually.  Right now, that means that you need to have an SSH key setup at the Bitbucket repo for your machine.

To start, be sure that you have [Composer](http://getcomposer.org) installed.

1. Create a directory for your new project.
2. Open a command prompt in your project directory and type `composer init` to create a blank composer project.  Follow the prompts to input your project name and other details.
3. Open your newly created `composer.json` file in the text editor of your choice and add the following lines underneath the "authors" node.

```json
	"repositories": [
		{
			"type": "git",
			"url":  "git@bitbucket.org:jenshort/oneframelink.git"
		}
	],
```

4. Include OFL by adding the following lines below the "repositories" node.  You may already have a "require" node; if so, just include the inner line within your existing node.

```json
    "require": {
        "jmulet/oneframelink": "dev-master",
    },
```

5. The final result should look something like the below:

```json
{
    "name": "My App",
    "authors": [
        {
            "name": "Jane Doe",
            "email": "doejn@myuniversity.edu"
        }
    ],
	"repositories": [
		{
			"type": "git",
			"url":  "git@bitbucket.org:jenshort/oneframelink.git"
		}
	],
    "require": {
    	"jmulet/oneframelink": "dev-master",
    },
    "autoload": {
        "psr-4": {
            "MyAppNamespace\\":"app"
        }
    }
}
```

6. Navigate to the project directory in your command prompt and type `composer install`.  This will download OFL and its dependencies to the vendor directory.

7. OFL looks for files in `app` and `public` directories.  You will need an `index.php` file in `public` to bootstrap the app.  Your project directory should look like this:
```
+-- composer.json
+-- app
|   +-- Config
|	|	+-- Routes.php
|	|	+-- Database.php
|   +-- Controller
|	|	+-- MyController.php
|	+-- Model
|	|	+-- MyModel.php
|	+-- View
|	|	+-- MyView.php
+-- public
|   +-- index.php
|   +-- styles.css
|   +-- .htaccess
+-- vendor
```