<?php

namespace OneFrameLink\Model;
use OneFrameLink\DbConnection;


class Model
{
	protected 	$fpdo,
				$data;

	public function __construct()
	{
		$db = DbConnection::connect();
		$this->fpdo = $db->fpdo;		
	}

	public function getData()
	{
		return $this->data;
	}
}