<?php

namespace OneFrameLink\Controller;

use OneFrameLink\Controller\Controller;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class MainController extends Controller
{
	public function index(ServerRequestInterface $request, ResponseInterface $response)
	{
		$template = $this->templates->make('main');
		$response->getBody()->write($template->render());

		return $response;	
	}

	public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
	{
		$template = $this->templates->make('main');
		$template->data(['data' => $args['id']]);
		$response->getBody()->write($template->render());

		return $response;	
	}	
}