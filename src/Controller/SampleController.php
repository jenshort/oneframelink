<?php

namespace OneFrameLink\Controller;

use OneFrameLink\Controller\Controller;
use OneFrameLink\Model\SampleModel;
use Symfony\Component\HttpFoundation\Request;	
use Symfony\Component\HttpFoundation\Response;

class SampleController extends Controller
{
	public function __construct()
	{
		parent::__construct();
		// These templates can be accessed via $this->templates->make(sample::<viewname)
		//$this->templates->addFolder('sample', '../app/View/samples');
	}

	public function show(Request $request, Response $response, array $args)
	{
		$report = new SampleModel;
		$data = $report->getData();
		$col_headers = $report->getHeaders();

		$response->setContent('This is the sample controller!');
		$response->setStatusCode(Response::HTTP_OK);

		return $response;	
	}

}