<?php
namespace OneFrameLink;

use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;
use \League\Container\Container;
use \Noodlehaus\Config;
use \League\Route\RouteCollection;

class App
{
  private $router,
          $dispatcher,
          $container;
  
  public function __construct()
  {
    
    $this->container = $this->setupContainer();
    if(!isset($this->router)) {
      $this->router = $this->setupRouter($this->container);
    }
  }
  
  private function setupContainer()
  {
    $container = new Container;

    $container->share('response', \Zend\Diactoros\Response::class);
    $container->share('request', function() {
      return \Zend\Diactoros\ServerRequestFactory::fromGlobals(
        $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
      );
    });
    
    // SAPI = Server API
    $container->share('emitter', \Zend\Diactoros\Response\SapiEmitter::class);
    return $container;
  }
  
  private function setupRouter($container)
  {
    $router = new RouteCollection($this->container);
    if(file_exists('../app/Config/Routes.php')) {
        $routes_config = Config::load('../app/Config/Routes.php');
    } else {
      // Note that your array in Routes.php should look basically like the one below
      $routes_config = [
        'routes' => [
          '/' => [
            [
              'verb' => 'GET',
              'path' => '',
              'action' => 'OneFrameLink\Controller\MainController::index'
            ]
          ]
        ]
      ];
    }
    $routes = $routes_config['routes'];
    $this->setupRoutes($routes, $router);   
    return $router;
  }
  
  private function setupRoutes($routes_config, $router)
  {
    foreach($routes_config as $route_group=>$routes) {
      $router->group($route_group, function($router) use($routes) {
        // echo "Inside the setupRoutes => group function!\n";
        foreach($routes as $route) {
          $router->map($route['verb'], $route['path'], $route['action']);
        }        
      });
    }
  }
  
  public function dispatch()
  {
    // dump($this->router);
    // dump($this->container->get('request'));
    // dump($this->container->get('response'));
    $response = $this->router->dispatch($this->container->get('request'), $this->container->get('response'));
    $this->container->get('emitter')->emit($response);
  }  
}