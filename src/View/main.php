<?php $this->layout('layout::layout') ?>
<div class="header">
	<h1>OneFrameLink is up and running!</h1>
	<p class="lead">Create your own landing page here.</p>
	<p>Views can be added to the <code>app/View</code> directory, and Routes can be added to <code>app/Config/Routes.php</code></p>

	<?php if(isset($data) && is_string($data)): ?>
		<p>The controller gave me <strong><?= $data ?></strong>!</p>
	<?php endif; ?>
</div>
